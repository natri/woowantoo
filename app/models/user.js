var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var request = require('request');

var Friend = require('./friend');
var FriendModel = Friend.model;

var encryption = require('../../module/encryption/encryption');

var NotFoundError = require('../NotFoundError');

var UserSchema = new Schema({
    public_profile: {
        lastname: String,
        firstname: String,
        birthdate: Date,
        gender: {type: String, enum: ['m', 'w']},
        photo: String,
        lang: String,
        city: String,
        country: String
    },
    //Credentials
    email: {type: String, index: {required: true}},
    password: String,
    api_key: String,
    fb_token: String,
    fb_id: {type: String, index: true}, //{type: String, index: {unique: true, required: true, dropDups: true}}
    gg_auth: String,
    //Friends
//    friends: {
//        approved: [Friend.schema],
//        toApprove: [{type: Schema.Types.ObjectId, ref: 'User'}],
//        sentDemands: [{type: Schema.Types.ObjectId, ref: 'User'}]
//    },
    
    friends_active: [{type: Schema.Types.ObjectId, ref: 'User'}], //store the active friend as a shorcut
    fb_friends: [{type: String}],
    
    //Filter    
    filter: {
        //EMIT
        sex: {type: String, default: 'all'},
        age_min: {type: Number, default: 0},
        age_max: {type: Number, default: 99},
            
        //RECEIVE
        explicit_content: {type: Boolean, default: Boolean.false},
        categories: [{type: String}],
        base_location: {type: String, default: ''},
        circ: {type: Number, default: 20}
    },
    //user can mask any activities
    hidden_activities: [{type: String, ref: 'Activity', index: true}],
    isActive: {type: Boolean, default: true},
});

/*****************************************************************************************************************
 * STATIC METHODS
 */



//Native App signin
UserSchema.statics.signin = function(authentication, callback) {
    this.findOne({"email": authentication.email}, function(err, user) {
        if (!user) {
            callback(new NotFoundError('user not found'), user);
            return;
        }

        encryption.compare(authentication.pwd, user.password, function(err, isAllowed) {

            console.log('Allowed: ' + isAllowed);
            if (isAllowed) {
                callback(err, user);
            } else {
                callback(new Error('not allowed'), user);
            }

        });

    });
};

//Native App signup
UserSchema.statics.signup = function(user, callback) {
    encryption.crypt(user.password, function(err, hash) {
        user.password = hash;
        user.save(callback);

    });
};

//Signin for users already stored in localstorage ->use api_key
UserSchema.statics.autoSignin = function(authentication, callback) {
    this.findById(authentication._id, function(err, user) {
        if (!user) {
            callback(new NotFoundError('user not found'), user);
            return;
        }

        if (user.api_key === authentication.api_key) {
            console.log('Allowed');
            callback(err, user);
        }
        else
        {
            console.log('Not Allowed');
            callback(new Error('not allowed'), null);
        }

    });
};


UserSchema.statics.findByEmail = function(email, callback) {
    //DO something
    //console.log(this);
    this.findOne({"email": email}, function(err, user) {
        if (!user)
            err = new NotFoundError("ID not found");
        callback(err, user);
    });
};

UserSchema.statics.findByFbId = function(fb_id, callback) {
    //DO something
    //console.log(this);
    this.findOne({"fb_id": fb_id}, function(err, user) {
        if (!user)
            err = new NotFoundError("ID not found");
        callback(err, user);
    });
};

//FB Signup
//UserSchema.statics.fb_signup = function(user, callback) {
//    user.extendFbToken(function(err, user) {
//        user.fb_sync(function(err, user) {
//            user.save(callback(err, user));
//        });
//    });
//};


/*****************************************************************************************************************
 *  INSTANCE METHODS
 */

////Update pwd
//UserSchema.methods.updatePassword = function() {
//        var that = this;
//        encryption.crypt(that.user.password, function(err, hash) {
//            that.user.password = hash;
//            that.user.save(callback);
//
//        });
//};

//Convert fb access token to a long live access token
UserSchema.methods.extendFbToken = function(callback) {

    if (this.fb_token) {
        console.log('TOKEN');
        console.log(this.fb_token);

        var params = '';
        params += 'grant_type=fb_exchange_token';
        params += '&client_id=' + '1488166564803051';
        params += '&client_secret=' + '765434b98219062dd43ff22b92f11676';
        params += '&fb_exchange_token=' + this.fb_token;

        var that = this;
        request('https://graph.facebook.com/oauth/access_token?' + params, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var fb_res = body.split('&');
                fb_res.forEach(function(ele, i) {
                    var fb_res_array = ele.split('=');
                    fb_res[i] = fb_res_array[1];
                });

                that.fb_token = fb_res[0];
                that.save(callback(null, that));
            }
        });
    }
};


//Sync native with fb profile
UserSchema.methods.fb_profile_sync = function(callback) {

    var that = this;
    
    var open_graph_url = 'https://graph.facebook.com/';
    var req_url = 'me/friends?fields=first_name,last_name,gender,birthday';
    var access_token = '&access_token=' + that.fb_token;

    if (this.fb_token) {
        var request = require('request');
        request(open_graph_url + req_url + access_token , function(error, response, body) {
            try {
                var body = JSON.parse(body);

                that.public_profile.photo = body.data.url;


            } catch (err) {
                new ErrorHandler(err, res);
            }

            that.save(callback(null, that));
        });
    }
};

//sync with FB profile
UserSchema.methods.fb_picture_sync = function(callback) {

    var that = this;

    if (this.fb_token) {
        var request = require('request');
        request('https://graph.facebook.com/me/picture?type=square&height=132&width=132&redirect=false&access_token=' + that.fb_token, function(error, response, body) {
            try {
                var body = JSON.parse(body);

                that.public_profile.photo = body.data.url;


            } catch (err) {
                new ErrorHandler(err, res);
            }

            that.save(callback(null, that));
        });
    }
};

//sync with FB profile
UserSchema.methods.fb_friends_sync = function(callback) {
    
    var open_graph_url = 'https://graph.facebook.com/';
    var req_url = 'me/friends?fields=first_name,last_name,gender,birthday';
    var access_token = '&access_token=' + that.fb_token;
    
    var that = this;

    if (this.fb_token) {
        var request = require('request');
        request(open_graph_url + req_url + access_token , function(error, response, body) {
            try {
                var body = JSON.parse(body);
               
                var friends = body.data;
                friends.forEach(function(friend){
                    that.fb_friends.addToSet(friend.id);
                });
                
            } catch (err) {
                new ErrorHandler(err, res);
            }

            that.save(callback(null, that));
        });
    }
};

//sync with FB profile
UserSchema.methods.genApiKey = function(callback) {
    var that = this;
    var random = this._id + new Date().getTime();
    encryption.crypt(random, function(err, hash) {
        if (err)
            callback(err, that);
        else {
            console.log(hash);
            that.api_key = hash;
            that.save(function(err, user) {
                callback(err, user);
            });
        }
    });
};

//UserSchema.methods.getFriend = function(friend_id,callback) {
//    this.isActive = false;
//    this.friends.approved.forEach(function(friend){
//        if(friend._user === friend_id){
//            callback(err, friend);
//            return;
//        }
//    });
//};


UserSchema.methods.closeAccount = function(callback) {
    this.isActive = false;
    this.findOneAndUpdate({_id: this._id}, this, {new : true, upsert: false}, function(err, activity) {
        if (!activity)
            err = new NotFoundError("ID not found");
        callback(err, activity);
    });
};

//UserSchema.methods.add_contacts = function(user, callback) {
//    this.contacts.addToSet(user);
//
//    var conditions = {id: this.id};
//    this.findOneAndUpdate(conditions, this, callback);
//    console.log('add_contacts');
//};

module.exports.schema = UserSchema;
module.exports.model = mongoose.model('User', UserSchema);