var https = require('https');

exports.getFbData = function(accessToken, apiPath, callback) {
    var options = {
        host: 'graph.facebook.com'
        , port: 443,
        path: apiPath + '?access_token=' + accessToken, //apiPath example: '/me/friends'        
        method: 'GET'};

    var buffer = '';//this buffer will be populated with the chunks of the data received from facebookvar
    
    request = https.get(options, function(result) {
        result.setEncoding('utf8');
        
        // Store chunks in in the buffer
        result.on('data', function(chunk) {
            buffer += chunk;
        });
        
        //Send the buffer when finished
        result.on('end', function() {
            callback(buffer);
        });
    });
    
    request.on('error', function(e) {
        console.log('error from facebook.getFbData: ' + e.message)
    });
    
    request.end();
};