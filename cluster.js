/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



cluster = require('cluster');


if (cluster.isMaster) {

    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function(worker) {
        console.log('Worker ' + worker.id + ' died :(');
        cluster.fork();

    });


}
else {
    var express = require('express');
    var app = express();

    app.get('/', function(req, res) {
        
        //TODO app.js
        res.send('Hello World!');
    });

    app.listen(3000);
    console.log('Application running!');

}
