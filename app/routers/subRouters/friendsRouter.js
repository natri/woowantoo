/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var express = require('express');
var NotFoundError = require('../../NotFoundError');
var ErrorHandler = require('../../handler/errorHandler');

//Services
var US = require('../../services/userServices');

//Models
var ActivityModule = require('../../models/activity');
var Activity = ActivityModule.model;

//helpers
var woo_tools = require('../../../module/wootools/wootools');


// ROUTES - FB Actions
// =============================================================================

// create our router
var friendsRouter = express.Router();

// middleware called before each routes in this router
friendsRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Friends router -> .' + req.originalUrl);
    next();
});

var root = '/:user_id/friends';

//ROUTES -- /:user_id/friends
friendsRouter.route( root )
        //Get all friends
        .get(function(req, res) {
            var user_id = req.params.user_id;
            
            US.getApprovedFriends(user_id, function(err, users){
                if(err)
                    new ErrorHandler(req, res);
                res.json(users);
            });
        });
        
        //ROUTES -- /:user_id/friends
friendsRouter.route( root +'/all/ids')
        //Get all friends
        .get(function(req, res) {
            var user_id = req.params.user_id;
            
            US.getAllFriendsIds(user_id, function(err, ids){
                if(err)
                    new ErrorHandler(req, res);
                res.json(ids);
            });
        });
//WARNING: 
friendsRouter.route( root + '/refresh')
        //Refresh the active embed in the user
        .get(function(req, res) {
            var user_id = req.params.user_id;
            
            US.refreshActiveFriends(user_id, function(err, user){
                if(err)
                    new ErrorHandler(req, res);
                res.json(user);
            });
        });
        
friendsRouter.route( root + '/qualify')
        //get the demand
        .post(function(req, res) {
            var user_id = req.params.user_id;
            var data = woo_tools.getData(req, res);
            
            
            US.activateFriends(user_id, data.ids, function(err, user){
                if(err)
                    new ErrorHandler(req, res);
                res.json(user);
            });
        });

friendsRouter.route( root + '/toapprove')
        //get the demand
        .get(function(req, res) {
            var user_id = req.params.user_id;
            
            US.getFriendToApprove(user_id, function(err, user){
                if(err)
                    new ErrorHandler(req, res);
                res.json(user);
            });
        });
        
friendsRouter.route( root + '/embed_active')
        //Refresh the active embed in the user
        .get(function(req, res) {
            var user_id = req.params.user_id;
            
            US.getEmbedFriends(user_id, function(err, user){
                if(err)
                    new ErrorHandler(req, res);
                res.json(user);
            });
        });
       
        
friendsRouter.route( root + '/:target_user')
        //make a demand
        .post(function(req, res) {
            var user_id = req.params.user_id;
            var target_user = req.params.target_user;
            
            US.askAsFriend(user_id, target_user, function(err, user){
                if(err)
                    new ErrorHandler(req, res);
                res.json(user);
            });
        })
        
        //delete :target_user from friends
        .delete(function(req, res) {
            var user_id = req.params.user_id;
            var target_user = req.params.target_user;
    
            US.removeFriend(user_id, target_user, function(err, friend){
                if(err)
                    new ErrorHandler(req, res);
                res.json(friend);
            });
        });
        
friendsRouter.route( root + '/:target_user/approve')
        //approved
        .put(function(req, res) {
            var user_id = req.params.user_id;
            var target_user = req.params.target_user;
            
            US.approveFriend(user_id, target_user, function(err, user){
                if(err)
                    new ErrorHandler(req, res);
                res.json(user);
            });
        });
        
        


module.exports = friendsRouter;

  