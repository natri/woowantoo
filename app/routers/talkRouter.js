/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var http = require('http');
var express = require('express');
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

//Services
var TS = require('../services/talkServices');

//helpers
var woo_tools = require('../../module/wootools/wootools');


// ROUTES - FB Actions
// =============================================================================

// create our router
var talkRouter = express.Router();

// middleware called before each routes in this router
talkRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Talk router -> .' + req.originalUrl);
    next();
});



var root = '/:user_id/activities/:act_id/talks';

talkRouter.route( root )

        //Get all talks
        .get(function(req, res) {
            var user_id = req.params.user_id;
            var act_id = req.params.act_id;
            
            TS.getTalks(act_id, function(err, talk){
                if(err)
                    new ErrorHandler(err, res);
                res.json(talk);
            });
        })
        
        //Add talk
        .post(function(req, res) {
            var user_id = req.params.user_id;
            var act_id = req.params.act_id;
            
            var talkData  = woo_tools.getData(req, res);
             
            TS.create(act_id, user_id, talkData, function(err, talk){
                if(err)
                    new ErrorHandler(err, res);
                res.json(talk);
            });
            
        });
        

module.exports = talkRouter;

  