/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var express = require('express');
var NotFoundError = require('../../app/NotFoundError');
var ErrorHandler = require('../../app/handler/errorHandler');

var Authentication = require('../../middleware/authentication/authentication');

var UserModule = require('../../app/models/user');
var User = UserModule.model;


// ROUTES FOR OUR API
// =============================================================================

// create our router
var signRouter = express.Router();

// middleware called before each routes in this router
signRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Sign router -> .' + req.originalUrl);
    next();
});

//signRouter.use(Authentication);

signRouter.route('/fb_signin')
        //Route to add an activity
        .post(function(req, res) {

        });

signRouter.route('/gg_signin')
        //Route to add an activity
        .post(function(req, res) {

        });

signRouter.route('/signin/:email/:pwd')
        .get(function(req, res) {

            var email = req.params.email;
            var pwd = req.params.pwd;

            var authentication = {
                email: email,
                pwd: pwd
            };

            User.signin(authentication, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(user);
            });

        });

signRouter.route('/signup')
        //Route to add an activity
        .post(function(req, res) {

            try {
                var userData = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }

            var user = new User(userData);

            User.signup(user, function(err, newuser) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(newuser);
            });
        });

signRouter.route('/fb_signup')
        //Route to add an activity
        .post(function(req, res) {

            try {
                var userData = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }

            var user = new User(userData);
            
            User.fb_signup(user, function(err, newuser) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(newuser);
            });
        });

module.exports = signRouter;

  