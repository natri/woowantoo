/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var http = require('http');
var express = require('express');
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var Authentication = require('../../middleware/authentication/authentication');

var encryption = require('../../module/encryption/encryption');

//var UserModule = require('../models/user');
//var User = UserModule.model;
//
//var ActivityModule = require('../models/user');
//var Activity = ActivityModule.model;
//
//var FriendModule = require('../models/friend');
//var Friend = FriendModule.model;
//
//var NotificationModule = require('../models/notification');
//var Notification = NotificationModule.model;


//Services
var US = require('../services/userServices');


//helpers
var woo_tools = require('../../module/wootools/wootools');

//SUB ROUTERS
var friendsRouter = require('./subRouters/friendsRouter');
var fb_actionsRouter = require('./subRouters/fb_actionsRouter');
var activityRouter = require('./subRouters/activityRouter');

//var notificationsRouter = require('./subRouters/notificationsRouter');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var userRouter = express.Router();

var current_user;

userRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - user router -> .' + req.originalUrl);
    next();
});

var root = '';



//CRUD Operation -------------------------------------------------------------------------
userRouter.route(root+ '/')
        //Add an user
        .post(function(req, res) {
            var data = woo_tools.getData(req, res);

            US.create(data, function(err, user) {
                if(err) 
                   new ErrorHandler(err, res);
                res.json(user);
            });
        });

//CRUD Operation -------------------------------------------------------------------------
userRouter.route(root+ '/:user_id')
        .get(function(req, res) {
            var user_id = req.params.user_id;
            US.getPublic(user_id, function(err, user) {
                if(err) 
                   new ErrorHandler(err, res);
                res.json(user);
            });
        })

        //Add an user
        .put(function(req, res) {
            var user_id = req.params.user_id;
            var data = woo_tools.getData(req, res);

            US.update(user_id, data, function(err, user) {
                if(err) 
                   new ErrorHandler(err, res);
                res.json(user);
            });
        })
        
        //Add an user
        .delete(function(req, res) {
            var user_id = req.params.user_id;

            US.update(user_id, {isActive: false}, function(err, user) {
                if(err) 
                   new ErrorHandler(err, res);
                res.json(user);
            });
        });
        
        
        


//Filter Operation -------------------------------------------------------------------------
userRouter.route(root + '/:user_id/filter/')
        //update filter
        .put(function(req, res) {
            var user_id = req.params.user_id;
            var data = woo_tools.getData(req, res);
            var updFilter = {filter: data};

            US.update(user_id, updFilter, function(err, user) {
                if(err) 
                   new ErrorHandler(err, res);
                res.json(user);
            });
        });

//Specific routers 
userRouter.use('/', friendsRouter);
userRouter.use('/', activityRouter);
userRouter.use('/', fb_actionsRouter);



module.exports = userRouter;

  