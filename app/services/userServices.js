/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//utility
var util = require("util");
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var encryption = require('../../module/encryption/encryption');

//MODELS
var ActivityModule = require('../models/activity');
var Activity = ActivityModule.model;

var UserModule = require('../models/user');
var User = UserModule.model;

var FriendModule = require('../models/friend');
var Friend = FriendModule.model;


var UserService = function() {
//    this.req = req;
//    this.res = res;
//    this.data = JSON.parse(req.body.data);
};

/*************************************************************************************************
 * CRUD SERVICES
 */

UserService.prototype.get = function(user_email, callback) {
    User.findByEmail(user_email, function(err, user) {
        callback(err, user);
    });
};

UserService.prototype.getPublic = function(user_id, callback) {
    var public = {_user: user_id};

    var option = {
        path: '_user',
        select: 'public_profile'
    };

    User.populate(public, option, function(err, user) {
        callback(err, user._user);
    });
};

UserService.prototype.create = function(data, callback) {
    var user = new User(data);

    if (user.password)
        encryption.crypt(user.password, function(err, hash) {
            user.password = hash;
            user.save(function(err, user) {
                callback(err, user);
            });
        });
    else
        user.save(function(err, user) {
            callback(err, user);
        });
};

UserService.prototype.update = function(user_id, updUser, callback) {

    var condition = {
        "_id": user_id
    };

    if (updUser._id)
        delete updUser._id;

    //Crypt the new password
    if (updUser.password) {
        encryption.crypt(updUser.password, function(err, hash) {
            updUser.password = hash;
            //update the user
            User.findOneAndUpdate(condition, updUser, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    callback(err, user);
            });
        });
    }
    else {
        //update the user
        User.findOneAndUpdate(condition, updUser, function(err, user) {
            if (err)
                new ErrorHandler(err, res);
            else
                callback(err, user);
        });
    }
};

UserService.prototype.unactive = function(user_id, callback) {
    User.findOneAndUpdate({_id: user_id}, {isActive: false}, function(err, user) {
            if (err)
                new ErrorHandler(err, res);
            else{
                //unactive all friend obj that point to the unactivate user
                Friend.update({_friend: user_id}, {isActive: false}, function(err, friend){
                        callback(err, user);
                });
            }
                
        });
};




/*************************************************************************************************
 * FRIEND SERVICES
 */

UserService.prototype.getEmbedFriends = function(user_id, callback) {
    var populate_option = {
        path: 'friends_active',
        select: 'public_profile'
    };

//    User.findById(user_id).populate(populate_option).exec(function(err, user) {
//        callback(err, user);
//    });
    
    User.findById(user_id, function(err, user) {
        User.populate(user, populate_option, function(err, user){
            callback(err, user.friends_active);
        });
        
    });
};

UserService.prototype.activateFriends = function(user_id, friend_ids, callback) {
     var condition = {
         _owner: user_id,
         state: 'approved'
     };
    
    Friend.find(condition, null, null, function(err, friends) {
        friends.forEach(function(friend){
            
            friend.isActive = false;
            if(friend_ids instanceof Array)
                friend_ids.forEach(function(friend_id){
                    console.log(friend);
                    console.log(friend_id);
                    if(friend._friend == friend_id){
                        console.log('match');
                        friend.isActive = true;
                        friend.save();
                        return;
                    }
                });
            friend.save();
        });
        
        callback(err, friends);
    });
};

UserService.prototype.refreshActiveFriends = function(user_id, callback) {
    var select_option = {
        path: '',
        select: '_id'
    };

    User.findById(user_id, function(err, user) {
        Friend.get({_owner: user_id, state: 'approved', isActive: true}, null, null, function(err, friends) {
            console.log(friends);
            var ids = [];
            user.friends_active = [];
            friends.forEach(function(friend){
                //addToSet if friend exist
                if(friend._friend)
                    user.friends_active.addToSet(friend._friend._id);
            });
            
            user.save(callback);
        });
    });
};

UserService.prototype.getFriendDemand = function(user_id, friend_id, callback) {
    Friend.get({_owner: user_id, state: 'in_demand'}, null, null, function(err, friends) {
        callback(err, friends);
    });
};

UserService.prototype.getFriendToApprove = function(user_id, callback) {
    Friend.get({_owner: user_id, state: 'action_required'}, null, null, function(err, friends) {
        callback(err, friends);
    });
};

UserService.prototype.getApprovedFriends = function(user_id, callback) {
    Friend.get({_owner: user_id, state: 'approved'}, null, null, function(err, friends) {
        callback(err, friends);
    });
};

UserService.prototype.getAllFriends = function(user_id, callback) {
    Friend.get({_owner: user_id}, null, null, function(err, friends) {
        callback(err, friends);
    });
};

UserService.prototype.getAllFriendsIds = function(user_id, callback) {
    
    var ids = [];
    
    Friend.find({_owner: user_id}, null, null, function(err, friends) {
        friends.forEach(function(friend){
            ids.push(friend._friend);
        });
        callback(err, ids);
        
    });
};


UserService.prototype.askAsFriend = function(user_id, friend_id, callback) {

    var condition = {
        $or: [{_owner: user_id, _friend: friend_id}, {_owner: friend_id, _friend: user_id}]
    };

    //check if a relation already exist
    Friend.get(condition, null, null, function(err, friends) {
        if (err)
            callback(err, friends);
        console.log('inner func');
        console.log(friends);

        if (friends.length === 0) {
            var src_friend = new Friend({
                _owner: user_id,
                _friend: friend_id,
                state: 'in_demand'
            });

            var target_friend = new Friend({
                _owner: friend_id,
                _friend: user_id,
                state: 'action_required'
            });

            src_friend.save(function(err, src_friend) {
                target_friend.save(function(err, target_friend) {
                    callback(err, [src_friend, target_friend]);
                });
            });
        }
        else
            callback(err, {error: 'already exist'});

    });

    console.log('outer');
};



UserService.prototype.approveFriend = function(user_id, friend_id, callback) {
    Friend.getOne({_owner: user_id, _friend: friend_id, state: 'action_required'}, null, null, function(err, friend) {
        console.log(friend);

        friend.state = 'approved';

        friend.save(function(err, friend) {
            //inverse operation to update the target friend
            Friend.getOne({_owner: friend_id, _friend: user_id, state: 'in_demand'}, null, null, function(err, target_friend) {
                target_friend.state = 'approved';
                target_friend.save(function(err, target_friend) {
                    callback(err, target_friend);
                });
            });
        });
    });
};


UserService.prototype.denyFriend = function(user_id, friend_id, callback) {

    Friend.getOne({_owner: user_id, _friend: friend_id, state: 'action_required'}, null, null, function(err, friend) {
        friend.state = 'denied';

        friend.save(function(err, friend) {
            //inverse operation to update the target friend
            Friend.getOne({_owner: friend_id, _friend: user_id, state: 'action_required'}, null, null, function(err, target_friend) {
                target_friend.state = 'denied';
                target_friend.save(callback);
            });
        });
    });
};

UserService.prototype.removeFriend = function(user_id, friend_id, callback) {
    var condition = {$or:[{_owner: user_id, _friend: friend_id}, {_owner: friend_id, _friend: user_id} ]};
    
    Friend.remove(condition ,function(err){
        callback(err, {});
    });
};



//UserService.prototype.removeFriend = function(user_id, friend_id, callback) {
//
//    //update the user 
//    User.findById(user_id, function(err, src_user) {
//        if (err)
//            new ErrorHandler(err);
//        src_user.getFriend(friend_id, function(err, friend) {
//            src_user.friends.approved.remove(friend);
//        });
//
//        //update the friends
//        src_user.save(function(err, user) {
//            User.findById(friend_id, function(err, friend_user) {
//                if (err)
//                    new ErrorHandler(err);
//                friend_user.getFriend(user_id, function(err, friend) {
//                    friend_user.friends.approved.remove(friend);
//                });
//            });
//        });
//    });
//};


module.exports = new UserService();