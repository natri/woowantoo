/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var DevLogger = require('./lib/devLogger');
var ProdLogger = require('./lib/prodLogger');

//The front logger middleware
// called before all routes
var frontLogger = function(req, res, next){
    new DevLogger(req, res, next);
}

module.exports = frontLogger;