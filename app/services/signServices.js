/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//utility
var util = require("util");
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var encryption = require('../../module/encryption/encryption');

//MODELS
var ActivityModule = require('../models/activity');
var Activity = ActivityModule.model;

var UserModule = require('../models/user');
var User = UserModule.model;

var FriendModule = require('../models/user');
var Friend = FriendModule.model;


var SignService = function() {
//    this.req = req;
//    this.res = res;
//    this.data = JSON.parse(req.body.data);
};

/*************************************************************************************************
 * CRUD SERVICES
 */

SignService.prototype.signin = function(authentication, callback) {

    User.signin(authentication, function(err, user) {
        callback(err, user);
    });
};

SignService.prototype.autoSignin = function(authentication, callback) {

    User.autoSignin(authentication, function(err, user) {
        callback(err, user);
    });
};



SignService.prototype.signup = function(data, callback) {
    var user = new User(data);

    if (user.password)
        encryption.crypt(user.password, function(err, hash) {
            if (err)
                callback(err, user);
            else {
                console.log(hash);
                user.password = hash;
//                user.save(function(err, user) {
//                    callback(err, user);
//                });
                user.genApiKey(function(err, user) {
                    callback(err, user);
                });

            }
        });
    else
        user.save(function(err, user) {
            callback(err, user);
        });
};


//Facebook
SignService.prototype.fb_signin = function(fb_user, callback) {
    
    console.log('fb_signin');
    
//    Check if the user is already registered
    User.findByEmail(fb_user.email, function(err, user) {
        //if found
        if (user) {
            console.log('user found');
            
            user.fb_id = fb_user.id;
            user.fb_token = fb_user.token;
            user.save(callback);
        }else{
            
            //Create a new user 
            console.log('create new user');
            var newuser = new User(fb_user);
            
            newuser.extendFbToken(function(err, newuser){
                newuser.fb_picture_sync(function(err, newuser){
                    newuser.genApiKey(callback);
                });
            }); 
        }
        
    });
};



module.exports = new SignService();