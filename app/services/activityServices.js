/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//utility
var util = require("util");
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var woo_tools = require('../../module/wootools/wootools');


//MODELS
var ActivityModule = require('../models/activity');
var Activity = ActivityModule.model;

var UserModule = require('../models/user');
var User = UserModule.model;

var FriendModule = require('../models/user');
var Friend = FriendModule.model;


var ActivityService = function() {
//    this.req = req;
//    this.res = res;
//    this.data = JSON.parse(req.body.data);
};

/*************************************************************************************************
 * CRUD SERVICES
 */

//ActivityService.prototype.get = function(activity_id, callback) {
//    var option = {path: 'sender', }
//    Activity.findById(activity_id).populate('entries').exec(function(err, activity) {
//        if (err)
//            callback(err, activity);
//        else
//            activity.getSender(function(err, build_activity) {
//                callback(err, build_activity);
//            });
//    });
//};

ActivityService.prototype.getById = function(activity_id, callback) {
    Activity.getById(activity_id, function(err, activity) {
        callback(err, activity);
    });
};


ActivityService.prototype.create = function(data, callback) {

    var activity = new Activity(data);

    activity.save(function(err, activity) {
        callback(err, activity);
    });
};

ActivityService.prototype.update = function(activity_id, updActivity, callback) {

    if (updActivity._id)
        delete updActivity._id;

    var condition = {_id: activity_id};
    Activity.findOneAndUpdate(condition, updActivity, function(err, activity) {
        callback(err, activity);
    });
};



/*************************************************************************************************
 * SEARCH SERVICES
 */

//Agenda
ActivityService.prototype.search_agenda = function(user_id, callback) {

    //prepare
    var date_condition = {"$gte": new Date()};
    var conditions = {
        date: date_condition,
        isActive: true,
        $or: [{'_sender': user_id}, {'suscriptions': user_id}]
    };
    var sort = {sort: 'date'};

    //the search func
    Activity.get(conditions, null, sort, function(err, activities) {
        callback(err, activities);
    });
};

//history
ActivityService.prototype.search_history = function(user_id, callback) {

    //prepare
    var date_condition = {"$lt": new Date()};
    var conditions = {
        date: date_condition,
        isActive: true,
        $or: [{'_sender': user_id}, {'suscriptions': user_id}]
    };
    var sort = {sort: 'date'};

    //the search func
    Activity.get(conditions, null, sort, function(err, activities) {

        callback(err, activities);
    });
};

//Friends
ActivityService.prototype.search_friends = function(user_id, callback) {

    //prepare
    var date_condition = {"$gte": new Date()};
    var conditions = {
        date: date_condition,
        isActive: true,
        'destinations': user_id,
        'suscriptions': {'$ne': user_id}
    };
    var sort = {sort: 'date'};

    //the search func
    Activity.get(conditions, null, sort, function(err, activities) {
        callback(err, activities);
    });
};

ActivityService.prototype.search_geo = function(user_id, location, callback) {

    User.findById(user_id, function(err, user) {
        Activity.geo(user, location, function(err, activities) {
            var option = {
                path: 'obj._sender',
                select: 'public_profile'
            };

            User.populate(activities, option, function(err, activities) {
                callback(err, activities);
            });
        });
    });


};

/*************************************************************************************************
 * ACTIVE STATE SERVICES
 */

ActivityService.prototype.unactive = function(activity_id, callback) {
    Activity.findOneAndUpdate({_id: activity_id}, {isActive: false}, function(err, activity) {
        callback(err, activity);
    });
};

/*************************************************************************************************
 * TALK SERVICES
 */

//ActivityService.prototype.getTalks = function(activity_id, callback) {
//    Activity.findById(activity_id, function(err, activity) {
//        if (err)
//            callback(err, null);
//        else {
//            activity.getTalks(function(err, talks) {
//                //res.json(talks);
//                callback(err, talks);
//            });
//        }
//    });
//};
//
//ActivityService.prototype.addTalk = function(activity_id, data, callback) {
//
//    Activity.findById(activity_id, function(err, activity) {
//        if (err)
//            callback(err, null);
//        else {
//            activity.addTalk(data, function(err, activity) {
//                callback(err, activity);
//            });
//        }
//    });
//};

/*************************************************************************************************
 * SUSCRIPTION SERVICES
 */

ActivityService.prototype.suscribe = function(activity_id, user_id, callback) {
    //contruct the talks obj

    Activity.getById(activity_id, function(err, activity) {
        if (err)
            callback(err, activity);
        else
            activity.addSubscription(user_id, function(err, activity) {
                callback(err, activity);
            });
    });
};

ActivityService.prototype.unsuscribe = function(activity_id, user_id, callback) {

    Activity.getById(activity_id, function(err, activity) {
        if (err)
            callback(err, activity);
        else
            activity.delSubscription(user_id, function(err, activity) {
                callback(err, activity);
            });

    });
};

module.exports = new ActivityService();