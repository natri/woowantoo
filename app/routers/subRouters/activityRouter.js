/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var express = require('express');
var NotFoundError = require('../../NotFoundError');
var ErrorHandler = require('../../handler/errorHandler');

//Services
var AS = require('../../services/activityServices');

//Models
var ActivityModule = require('../../models/activity');
var Activity = ActivityModule.model;

//helpers
var woo_tools = require('../../../module/wootools/wootools');

//subRouter
//var talksRouter = require('./talksRouter');


// ROUTES - FB Actions
// =============================================================================

// create our router
var activityRouter = express.Router();

// middleware called before each routes in this router
activityRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Activity router -> .' + req.originalUrl);
    next();
});

var root = '/:user_id/activities';

        
//CRUD Operation -------------------------------------------------------------------------
activityRouter.route( root )
        //Add an activity
        .post(function(req, res) {
          var user_id = req.params.user_id;
           var data = woo_tools.getData(req, res);
           
           data._sender = user_id;
            
            AS.create(data ,function(err, activity){
                if(err) 
                   new ErrorHandler(err, res);
                res.json(activity);
            });
        });

        
//CRUD Operation (suite)-------------------------------------------------------------------------
activityRouter.route( root + '/:act_id/')
        //Get a specific activity
        .get(function(req, res) {
            var act_id = req.params.act_id;
            
            AS.getById(act_id, function(err, activity){
                if(err) 
                   new ErrorHandler(err, res);
                res.json(activity);
            });
        })  
        
        //update
        .put(function(req, res) {
            var act_id = req.params.act_id;
            var data = woo_tools.getData(req, res);
            
            AS.update(act_id, data ,function(err, activity){
                if(err) 
                   new ErrorHandler(err, res);
                res.json(activity);
            });
        })
        
        //remove
        .delete(function(req, res) {
            //NOTHING TODO
        });
       
        
        
//Search -----------------------------------------------------------------------
activityRouter.route( root + '/search/')
        //Get all activities
        .get(function(req, res) {
            var user_id = req.params.user_id;
            
            res.json({
                src_usr: src_usr,
                data: 'All'
            });
        });

activityRouter.route( root + '/search/agenda')        
        //Get all activities
        .get(function(req, res) {
            var user_id = req.params.user_id;
            AS.search_agenda(user_id, function(err, activities){
                if(err)
                    new ErrorHandler(err, res);
                
                res.json(activities);
            });
        });
        
activityRouter.route( root + '/search/history')        
        //Get all activities
        .get(function(req, res) {
            var user_id = req.params.user_id;
            AS.search_history(user_id, function(err, activities){
                if(err)
                    new ErrorHandler(err, res);
                
                res.json(activities);
            });
        });
        
activityRouter.route( root + '/search/friends')           
        //Get all activities
        .get(function(req, res) {
            var user_id = req.params.user_id;
            AS.search_friends(user_id, function(err, activities){
                res.json(activities);
            });
        });
        
activityRouter.route( root + '/search/geo')           
        //Get all activities
        .get(function(req, res) {
            var user_id = req.params.user_id;
            var data = req.query.data;
            
            try{
                data = JSON.parse(data);
            }catch(err){
                new ErrorHandler(err, res);
            }
            var location = data;
            
            AS.search_geo(user_id, location, function(err, activities){
                res.json(activities);
            });
        });
        

//Suscription -------------------------------------------------------------------------
activityRouter.route( root + '/:act_id/suscriptions')
        //get all
        .get(function(req, res) {
            var user_id = req.params.user_id;
            var act_id = req.params.act_id;
            
            res.json({
                act_id: act_id,
                data: 'get All suscriptions'
            });
        }) 

        //Suscribe
        .post(function(req, res) {
            var user_id = req.params.user_id;
            var act_id = req.params.act_id;
            
            AS.suscribe(act_id, user_id, function(err, activity){
                if(err)
                    new ErrorHandler(err, res);
                
                res.json(activity);
            });
            
        }) 
        
        //Unsuscribe
        .delete(function(req, res) {
            var user_id = req.params.user_id;
            var act_id = req.params.act_id;
            
            AS.unsuscribe(act_id, user_id, function(err, activity){
                if(err)
                    new ErrorHandler(err, res);
                
                res.json(activity);
            });
        });
        
 activityRouter.route( root + '/:act_id/suscriptions/reject/:suscriber_id')       
        //Unsuscribe
        .delete(function(req, res) {
            var user_id = req.params.user_id;
    
            var suscriber_id = req.params.suscriber_id;
            var act_id = req.params.act_id;
            
            AS.unsuscribe(act_id, suscriber_id, function(err, activity){
                if(err)
                    new ErrorHandler(err, res);
                
                res.json(activity);
            });
        });

//Stats ------------------------------------------------------------------------
activityRouter.route( root + '/:act_id/stats')
        //get all
        .get(function(req, res) {
            var user_id = req.params.user_id;
            var act_id = req.params.act_id;
            
            res.json({
                act_id: act_id,
                data: 'get All suscriptions'
            });
        })         
        
        
       

module.exports = activityRouter;

  