/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ErrorHandler = require('../../app/handler/errorHandler');

//an helpers lib
var wootools = {
    activity: {
        build_senders: function(activities, callback) {
            var build_activities = [];
            if (activities.length > 0) {
                activities.forEach(function(item) {

                    //if Geo 
                    if (item.dis)
                        var activity = item.obj;
                    else
                        var activity = item;

                    //getSender DATA
                    activity.getSender(function(err, build_activity) {
                        //if Geo
                        if (item.dis)
                            build_activities.push({dis: item.dis, obj: build_activity});
                        else
                            build_activities.push(build_activity);

                        if (activities.length === build_activities.length) {
                            callback(build_activities);
                        }
                    });

                });
            }
            else {
                callback(build_activities);
            }
        }
    },
    getData: function(req, res) {
        var data = req.body.data;
        
        if (typeof data === 'string') {
            try {
                data = JSON.parse(req.body.data);
                return data;
            } catch (err) {
                new ErrorHandler(err, res);
                //return err;
            }
        }
        else{
            return data;
        }

    },
    calculateAge: function(birthday) { // birthday is a date
                var ageDifMs = Date.now() - birthday.getTime();
                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            }

};

module.exports = wootools;


