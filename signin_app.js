// BASE SETUP
// =============================================================================

//config
var config = require('./app/config');

// call the packages we need
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var app = express();

//DATABASE CONNECTION
mongoose.connect(config.db || 'mongodb://woowa:re4dy2db@ds053479.mongolab.com:53479/woowantoo'); // connect to our database

var port = process.env.PORT || 8080; // set our port

//Front Middleware
app.use(bodyParser());
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    // do logging
    console.log('SIGN MIDDLEWARE -> .'+req.originalUrl);
    console.log('APP: '+config.app);
    next();
});

//ROUTERS

////Default routes
//app.get('/',function(req, res){
//   res.send(config.app); 
//   console.log('default routes /');
//});

var signRouter = require('./signin/routers/signRouter');
app.use('/signin', signRouter);

// START THE SERVER
// =============================================================================
app.listen(port);

console.log('-----------------------------------------------------');
console.log('App "'+config.app+'" load on port: ' + port);
console.log('NODE_ENV: '+process.env.NODE_ENV || 'DEFAULT');
console.log('-----------------------------------------------------');
