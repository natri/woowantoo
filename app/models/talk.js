var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = require('./user');
var UserModel = User.model;


var TalkSchema = new Schema({
    _activity: { type: Schema.Types.ObjectId, ref: 'Activity' },
    _sender: { type: Schema.Types.ObjectId, ref: 'User' },

    message: String,
    date: {type: Date, default: Date.now}
});

// Sender
TalkSchema.statics.getById = function(talk_id, callback) {

    var option = {
        path: '_sender',
        select: 'public_profile'
    };
    
    this.findById(talk_id).populate(option).exec(function(err, talk) {
        callback(err, talk);
    });
};

//build activity - _sender
TalkSchema.statics.get = function(conditions, field, sort, callback) {
    
    var option = {
        path: '_sender',
        select: 'public_profile'
    };
    
    this.find(conditions, field, sort).populate(option).exec(function(err, talks) {
        callback(err, talks);
    });
}




/*****************************************************************************************************************
 *  INSTANCE METHODS
 */

//// Sender
//TalkSchema.methods.getSender = function(callback) {
//    var that = this;
//    UserModel.findByEmail(this.sender_email, function(err, user) {
//        //that.sender = user;
//        callback(err, user);
//    });
//};

module.exports.schema = TalkSchema;
module.exports.model = mongoose.model('talk', TalkSchema);