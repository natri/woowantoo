/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//utility
var util = require("util");
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var woo_tools = require('../../module/wootools/wootools');


//MODELS
var ActivityModule = require('../models/activity');
var Activity = ActivityModule.model;

var UserModule = require('../models/user');
var User = UserModule.model;

var TalkModule = require('../models/talk');
var Talk = TalkModule.model;


var TalkService = function() {
//    this.req = req;
//    this.res = res;
//    this.data = JSON.parse(req.body.data);
};

/*************************************************************************************************
 * TALK SERVICES
 */

TalkService.prototype.getTalks = function(activity_id, callback) {
    var conditions = {
        _activity: activity_id,
    };
    var sort = {sort: {'date': -1}};
    
    //the search func
    Talk.get(conditions, null, sort, function(err, activities) {
        callback(err, activities);
    });
};

TalkService.prototype.create = function(activity_id, user_id, data, callback) {
    var talk = new Talk(data);
    talk._activity = activity_id;
    talk._sender = user_id;
    
    talk.save(callback);
    
};

module.exports = new TalkService();